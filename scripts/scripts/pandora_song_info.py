#!/usr/bin/env python

"""
 Get song info from MPRIS dbus
 http://amhndu.github.io/Blog/python-dbus-mpris.html
"""
import dbus
import re


pithos_dbus = 'org.mpris.MediaPlayer2.pithos'
bus = dbus.SessionBus()
is_running = False

for service in bus.list_names():
    if re.match(pithos_dbus, service):
        is_running = True
        break

if is_running is False:
    print("")
else:
    player = dbus.SessionBus().get_object(pithos_dbus, '/org/mpris/MediaPlayer2')
    interface = dbus.Interface(player, dbus_interface='org.mpris.MediaPlayer2.Player')
    property_interface = dbus.Interface(player, dbus_interface='org.freedesktop.DBus.Properties')
    #for property, value in property_interface.GetAll('org.mpris.MediaPlayer2.Player').items():
    #    print(property,";",value)

    status = property_interface.Get('org.mpris.MediaPlayer2.Player', 'PlaybackStatus') 
    metadata = player.Get('org.mpris.MediaPlayer2.Player', 'Metadata', dbus_interface='org.freedesktop.DBus.Properties')

    title = metadata['xesam:title']
    artist = metadata['xesam:artist'][0]

    print("status:{}\ntitle:{}\nartist:{}".format(status,title, artist))

