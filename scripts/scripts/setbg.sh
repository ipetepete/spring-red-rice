#!/usr/bin/bash

# colorz or haishoku makes a better palette IMO
wal -i $@ --backend colorz
feh --bg-fill $@

# Set login screen background. I've config'd LightDM to grab this background file...
# Of course you'd have to make it writable for your user/group
#magick convert $@ /usr/share/backgrounds/custom.jpg


newcolor=$(cat ~/.cache/wal/colors.Xresources | grep "\*.color14" | cut -d " " -f 2)

# Theme the icons
for i in $(find ~/.config/awesome/themes/ -name "*.png"); do
  convert $i -fill "$newcolor" -colorize 100% $i
done

