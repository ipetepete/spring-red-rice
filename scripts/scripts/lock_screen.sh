#!/usr/bin/env bash

# check if audio is playing
if pacmd list-sink-inputs  | grep -w state | grep -q RUNNING ; then
  xdotool key XF86AudioPlay
fi

i3lock -c 000070
sleep 1
xset dpms force off

