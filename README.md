# Red Spring Rice

![Rofi](screenshots/rofi.png)
-----------
![Terminals](screenshots/terminals.png)
![Sidebar](screenshots/sidebar.png)

### Inspiration/Shoutouts

- [elenapan](https://github.com/elenapan/dotfiles) Awesome setup, great docs


### My setup:
- __OS__: Manjaro 18
- __WM__: AwesomeWM
- __Term__: Kitty
- __Theme__: Based on default Manjaro theme Cecious
- __Power Management__: TLP
- __Audio__: Pulseaudio
- __Backlight__: Light

### Dependancies:
- awesome v4.2+
- rofi
- feh
- wal
- imagemagick

## Recomendations

I've organized the files in this repo to be easy to setup using [stow](https://www.gnu.org/software/stow/) which is a program that easilly sets up symlinks from a given directory. [Brandon Invergo - Using GNU Stow to manage your dotfiles](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html) is a great writeup describing it's usage.

Basically all you need to do from the repo's root directory:
`stow rofi` will make a symlink in `~/.config/rofi` to the rofi directory in this repo.

To try the rice without the risk make a backup of your files i.e. `mv ~/.config/awesome  ~/.config/awesome.bak` then do `stow awesome` to try out my configs

## Customize your theme/icons

The included script `setbg.sh` will set your colors, wallpaper and colorize your icons to match:

      #!/usr/bin/bash

      # colorz or haishoku makes a better palette IMO
      wal -i $@ --backend colorz
      feh --bg-fill $@

      # Set login screen background. I've config'd LightDM to grab this background file...
      # Of course you'd have to make it writable for your user/group
      #magick convert $@ /usr/share/backgrounds/custom.jpg

      newcolor=$(cat ~/.cache/wal/colors.Xresources | grep "\*.color14" | cut -d " " -f 2)

      # Theme the icons
      for i in $(find ~/.config/awesome/themes/ -name "*.png"); do
        convert $i -fill "$newcolor" -colorize 100% $i
      done

